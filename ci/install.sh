#!/bin/bash

# Retrieve and parse all groups
groups=$(sudo docker run -v /vagrant:/app/parallelTests/ acme/parallelTests phpunit -c /app/parallelTests/app --list-groups)

parsed=$(echo $groups | sed "s/-/\t/g")
results=$(echo $parsed | awk '{for(i=9;i&lt;=NF;i++) {print $i}}')

# Loop on each group name and run parallel
for i in $results; do
   echo $i
done | parallel sudo docker run -v /vagrant:/app/parallelTests/ acme/parallelTests phpunit -c /app/parallelTests/app --group {}